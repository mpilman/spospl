#include "ptask_queues.h"

int front_test() {
    queue_st* q = queue_init(1);
  for (int i = 0; i < 10; i++) {
    char* c = (char*)malloc(256*1024*sizeof(char));
    sprintf(c, "This is a test %d", i);
    queue_add_tail(q, c);
  }
  
  for (int i = 0; i < 59000011; i++) {
    //queue_print(q, QT_STRING);
    char* x = (char*)queue_remove_front(q);
    queue_add_tail(q, (void*)x);
  }
  
  queue_print(q, QT_STRING);
  
  queue_deinit(q);
}

int back_test() {
    queue_st* q = queue_init(1);
  for (int i = 0; i < 10; i++) {
    char* c = (char*)malloc(256*1024*sizeof(char));
    sprintf(c, "This is a test %d", i);
    queue_add_tail(q, c);
  }
  
  for (int i = 0; i < 59000011; i++) {
    //queue_print(q, QT_STRING);
    char* x = (char*)queue_remove_tail(q);
    queue_add_tail(q, (void*)x);
  }
  
  queue_print(q, QT_STRING);
  
  queue_deinit(q);
}

int main(int argc, char** argv) {
 
front_test();
back_test();
  
  return 1;
}