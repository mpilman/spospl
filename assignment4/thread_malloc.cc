#include "thread_malloc.h"
#include "sparsehash/dense_hash_map"

#include <pthread.h>
#include <unordered_map>
#include <vector>
#include <malloc.h>
#include <cstdlib>

namespace {
pthread_key_t malloc_key;
constexpr std::size_t MAX_FREE_MEM = 0x6400000; // 100 MB
}

struct malloc_struct {
    google::dense_hash_map<std::size_t, std::vector<void*>> map;
    std::size_t allocated_size;
    malloc_struct() : allocated_size(0) {
        map.set_empty_key(0);
    }

    void *allocate(size_t size) {
        auto& l = map[size];
        if (l.empty())
            return malloc(size);
        void* ptr = l.back();
        l.pop_back();
        allocated_size -= size;
        return ptr;
    }

    void deallocate(void* ptr, std::size_t size) {
        if (allocated_size + size > MAX_FREE_MEM) {
            free(ptr);
            return;
        }
        auto& list = map[size];
        list.push_back(ptr);
        allocated_size += size;
    }
};

void free_malloc_struct_t(void *ptr)
{
    delete reinterpret_cast<malloc_struct*>(ptr);
}

malloc_struct *get_struct()
{
    malloc_struct *res = (malloc_struct*) pthread_getspecific(malloc_key);
    if (!res) {
        res = new malloc_struct();
        int err = pthread_setspecific(malloc_key, res);
        if (err) {
            printf("pthread_set_specific failed");
            exit(1);
        }
    }
    return res;
}

void thread_malloc_init()
{
    int err = pthread_key_create(&malloc_key, &free_malloc_struct_t);
    if (err) {
        printf("pthread_key_create failed");
        exit(1);
    }
}

void *thread_malloc(size_t size)
{
    malloc_struct* s = get_struct();
    return s->allocate(size);
}

void thread_free(void *ptr, size_t size)
{
    get_struct()->deallocate(ptr, size);
}

