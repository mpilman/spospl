project(assignment4)
cmake_minimum_required(VERSION 2.8)

#Sends the -std=c99 flag to the gcc compiler
add_definitions(-O0)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu99")

set(SRC_TASKS2 ptask_ucontext.c ptask_queues.c ptasks_main.c thread_malloc.h thread_malloc.cc)
include_directories(${CMAKE_SOURCE_DIR})

add_executable(ptask_ucontext ${SRC_TASKS2})

target_link_libraries(ptask_ucontext pthread)
