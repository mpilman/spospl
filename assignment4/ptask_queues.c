#include "ptask_queues.h"
#include "ptasks.h"
#include "thread_malloc.h"

#define LOCK (pthread_mutex_lock(q->lock))
#define UNLOCK (pthread_mutex_unlock(q->lock))

void queue_lock(queue_st* q) {
  LOCK;
}
void queue_unlock(queue_st* q) {
  UNLOCK;
}



queue_st* queue_init(int id) {
  queue_st* q = (queue_st*) thread_malloc(sizeof(queue_st));
  
  if (NULL == q) {
    fprintf(stderr, "Could not initialized the queue. Not enough memory!\n");
    return NULL;
  }
  
  q->lock = (pthread_mutex_t*) thread_malloc(sizeof(pthread_mutex_t));
  if (NULL == q->lock) {
    fprintf(stderr, "Could not initialized the queue. Not enough memory!\n");
    return NULL;
  }
  
  pthread_mutex_init(q->lock, NULL);
  q->head = q->tail = NULL;
  q->id = id;
  q->count = 0;
  return q;
}

void queue_deinit(queue_st* q) {
  if (NULL == q) return;
  while (q->head) {
    void* el = queue_remove_front(q);
    if (NULL != el) {
      free(el);
    }
  }
  if (NULL != q->lock)
    thread_free(q->lock, sizeof(pthread_mutex_t));
  thread_free(q, sizeof(queue_st));
}

void queue_print(queue_st* q, QUEUE_TYPE t) {
  LOCK;
  
  if (NULL != q) {
    
    printf("----- Queue contents [%d] [%u]: ----\n", q->id, TID);
    
    queue_elem_st* it = q->head;
    while (NULL != it) {
      switch (t) {
      case QT_JOBS:
	printf("id=%d, done=%d, sync=%d\n", ((ptask_st*)(it->data))->id, ((ptask_st*)(it->data))->done, ((ptask_st*)(it->data))->sync_tasks_count);
	ptask_st** tasks = ((ptask_st*)(it->data))->sync_tasks;
	for (int i = 0; i < ((ptask_st*)(it->data))->sync_tasks_count; i++) {
	  printf("\t id = %d, done = %d, arg = %u\n", tasks[i]->id, tasks[i]->done, tasks[i]->args);
	}
	break;
      case QT_STACKS:
	break;
      case QT_STRING:
	printf("%s\n", (char*)it->data);
	break;
      default:
	break;
      }
      it = it->next;
    }
    
    printf("+++++ Queue contents end\n");
    
  }
  UNLOCK;
}

unsigned queue_count(queue_st* q) {
  if (NULL == q) return 0;
  unsigned ret = 0;
  //LOCK;
  ret = q->count;
  //UNLOCK;
  return ret;
}

void queue_add_front(queue_st* q, void* data) {
  LOCK;
  if (NULL != q) {
    queue_elem_st* el = (queue_elem_st*) thread_malloc(sizeof(queue_elem_st));
    if (NULL == el) {
      fprintf(stderr, "Could not initialized the new queue element. Not enough memory!\n");
      goto exit;
    }
    el->data = data;
    el->next = NULL;
    el->prev = NULL;
    
    if (NULL == q->head && NULL == q->tail) {
      // we're adding the first element
      q->head = q->tail = el;
      q->count++;
    } else if (NULL != q->head && NULL != q->tail) {
      el->next = q->head;
      el->next->prev = el;
      q->head = el;
      q->count++;
    } else {
      // should never get here ...
      fprintf(stderr, "My poor coding skills got us here... :( \n");
      goto exit;
    }
  }
exit:
  UNLOCK;
  return;
}

void queue_add_tail(queue_st* q, void* data) {
  LOCK;
  if (NULL != q) {
    queue_elem_st* el = (queue_elem_st*) thread_malloc(sizeof(queue_elem_st));
    if (NULL == el) {
      fprintf(stderr, "Could not initialized the new queue element. Not enough memory!\n");
      goto exit;
    }
    el->data = data;
    el->next = NULL;
    el->prev = NULL;
    
    if (NULL == q->head && NULL == q->tail) {
      // we're adding the first element
      q->head = q->tail = el;
      q->count++;
    } else if (NULL != q->head && NULL != q->tail) {
      q->tail->next = el;
      el->prev = q->tail;
      q->tail = el;
      q->count++;
    } else {
      // should never get here ...
      fprintf(stderr, "My poor coding skills got us here... :( \n");
      goto exit;
    }
  }
exit:
  UNLOCK;
  return;
}

// remove from tail
void* queue_remove_tail(queue_st* q) {
  LOCK;
  void* data = NULL;
  if (NULL != q) {
    if (NULL == q->head && NULL == q->tail) {
      // list is empty ...
      goto exit;
    } else if (NULL != q->head && NULL != q->tail) {
      queue_elem_st* el = q->tail;
      data = el->data;
      
      q->tail = q->tail->prev;
      if (NULL != q->tail) q->tail->next = NULL;
      
      thread_free(el, sizeof(queue_elem_st));
      if (NULL == q->tail) {
	q->head = q->tail;
      }
      q->count--;
      goto exit;
    } else {
      // should never get here ...
      fprintf(stderr, "My poor coding skills got us here... :( \n");
      goto exit;
    }
  }
exit:
  UNLOCK;
  return data;
}

// remove from front
void* queue_remove_front(queue_st* q) {
  LOCK;
  void* data = NULL;
  if (NULL != q) {
    if (NULL == q->head && NULL == q->tail) {
      // list is empty ...
      goto exit;
    } else if (NULL != q->head && NULL != q->tail) {
      queue_elem_st* el = q->head;
      q->head = q->head->next;
      if (NULL!=q->head) q->head->prev = NULL;
      data = el->data;
      thread_free(el, sizeof(queue_elem_st));
      if (NULL == q->head) {
	q->tail = q->head;
      } 
      q->count--;
      goto exit;
    } else {
      // should never get here ...
      fprintf(stderr, "My poor coding skills got us here... :( \n");
      goto exit;
    }
  }
exit:
  UNLOCK;
  return data;
}
