#pragma once
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif
void thread_malloc_init();
void *thread_malloc(size_t size);
void thread_free(void *ptr, size_t size);
#ifdef __cplusplus
}
#endif

