#include "ptasks.h"
#include "thread_malloc.h"

queue_st* thread_queues[64];
pthread_t* threads[64];
volatile int stop_running;
unsigned long jobs_done[64];
int no_cores;
int no_threads;
volatile unsigned rr;
pthread_mutex_t global;


void* run (void*);
void task_consumer();
ptask_st* steal(unsigned long);

void print_stack() {
  void *trace[32];
  char **messages = (char **)NULL;
  int i, trace_size = 0;

  trace_size = backtrace(trace, 32);
  messages = backtrace_symbols(trace, trace_size);
  for (i=0; i<trace_size; ++i) {
    char syscom[256];
    sprintf(syscom,"echo -e \"\t%u\t%s\" | c++filt", TID, messages[i]); 
    system(syscom);
  }
}

void init_pools(int descriptors, int stacks) {
  
}

void ptask_print(ptask_st* task) {
  printf("\t[%u] id=%d, done=%d, sync=%d\n", TID, task->id, task->done, task->sync_tasks_count);
  ptask_st** tasks = task->sync_tasks;
  for (int i = 0; i < task->sync_tasks_count; i++) {
    printf("\t\t [%u] id = %d, done = %d, arg = %u\n", TID, tasks[i]->id, tasks[i]->done, tasks[i]->args);
  }
  printf("\t[%u] Task %d with stack pointers child = %p, parent = %p and contexts child = %p, parent = %p, link = %p \n", 
	 TID, (task)->id, 
	 (task)->context->uc_child->uc_stack.ss_sp, (task)->context->uc_parent->uc_stack.ss_sp,
	 (task)->context->uc_child, (task)->context->uc_parent, (task)->context->uc_child->uc_link);
}

void ptasks_init(unsigned _no_threads, unsigned _no_cores) { 
  stop_running = 0;
  
  no_threads = _no_threads;
  no_cores = _no_cores;
  
  for (unsigned i = 0; i < no_threads; i++) {
    thread_queues[i] = queue_init(i);
    threads[i] = (pthread_t*)malloc(sizeof(pthread_t));
    pthread_create(threads[i], NULL, run, (void*) i);
    jobs_done[i] = 0;
  }
  pthread_mutex_init(&global, NULL);
  rr = 0;
}

void ptasks_deinit() {
  for (int i = 0; i < no_threads; ++i) {
    queue_print(thread_queues[i], QT_JOBS);
    printf("Jobs done by th/q %d = %lu\n", i, jobs_done[i]);
  }
  
  stop_running = 1;
  for (int i = 0; i < no_threads; i++) {
    pthread_join(*threads[i], NULL);
  }
  for (int i = 0; i < no_threads; i++) {
    queue_deinit(thread_queues[i]);
    free(threads[i]);
  }
}

void wrapper_fun(ptask_st* task) {
  return_t ret = task->impl(task->args);
  *(task->ret) = ret;
  task->done = 1;
  setcontext(task->context->uc_parent);
}

void wrapper_sync(ptask_st* sync_task) {
  int all_done = 1;
  for (size_t i = 0; i < sync_task->sync_tasks_count && all_done; ++i) {
    if (!(sync_task->sync_tasks[i]->done)) {
      all_done = 0;
    }
  }
  if (all_done)  {
    sync_task->done = 1;
    setcontext(sync_task->context->uc_parent);
  } else {
    task_consumer();
    setcontext(sync_task->context->uc_parent);
  }
}



void* get_stack_malloc(size_t size) {
  void* stack = malloc(size);
  return stack;
}

void* get_stack_reuse(size_t size) {
  void* stack = thread_malloc(size);
  return stack;
}

void* get_stack(size_t size) {
#ifdef REUSE
  return get_stack_reuse(size);
#else
  return get_stack_malloc(size);
#endif
}

void release_stack_malloc(void* stack) {
  free(stack);
}

void release_stack_reuse(void* stack) {
  thread_free(stack, STACK_SIZE);
}

void release_stack(void* stack) {
#ifdef REUSE
  release_stack_reuse(stack);
#else
  release_stack_malloc(stack);
#endif
}



ptask_st* get_descriptor_malloc() {
  ptask_st* task = (ptask_st*) malloc(sizeof(ptask_st));
  
  (task)->context = (ptask_context_st*) malloc(sizeof(ptask_context_st));
  (task)->context->uc_child = (ucontext_t*) malloc(sizeof(ucontext_t));
  (task)->context->uc_parent = (ucontext_t*) malloc(sizeof(ucontext_t));
  
  
  (task)->context->uc_child->uc_stack.ss_flags = 0;
  (task)->context->uc_child->uc_stack.ss_size = STACK_SIZE;
  (task)->context->uc_child->uc_stack.ss_sp = get_stack_malloc(STACK_SIZE);
  
  (task)->context->uc_parent->uc_stack.ss_flags = 0;
  (task)->context->uc_parent->uc_stack.ss_size = STACK_SIZE;
  (task)->context->uc_parent->uc_stack.ss_sp = get_stack_malloc(STACK_SIZE);
  
  return task;
}

ptask_st* get_descriptor_reuse() {
  ptask_st* task = (ptask_st*) thread_malloc(sizeof(ptask_st));
  
  (task)->context = (ptask_context_st*) thread_malloc(sizeof(ptask_context_st));
  (task)->context->uc_child = (ucontext_t*) thread_malloc(sizeof(ucontext_t));
  (task)->context->uc_parent = (ucontext_t*) thread_malloc(sizeof(ucontext_t));
  
  
  (task)->context->uc_child->uc_stack.ss_flags = 0;
  (task)->context->uc_child->uc_stack.ss_size = STACK_SIZE;
  (task)->context->uc_child->uc_stack.ss_sp = get_stack_reuse(STACK_SIZE);
  
  (task)->context->uc_parent->uc_stack.ss_flags = 0;
  (task)->context->uc_parent->uc_stack.ss_size = STACK_SIZE;
  (task)->context->uc_parent->uc_stack.ss_sp = get_stack_reuse(STACK_SIZE);
  
  return task;
}

ptask_st* get_descriptor() {
#ifdef REUSE
  return get_descriptor_reuse();
#else
  return get_descriptor_malloc();
#endif
}

void release_descriptor_malloc(ptask_st* task) {
  release_stack_malloc(task->context->uc_parent->uc_stack.ss_sp);
  release_stack_malloc(task->context->uc_child->uc_stack.ss_sp);
  free(task->context->uc_parent);
  free(task->context->uc_child);
  free(task->context);  
  free(task);
}

void release_descriptor_reuse(ptask_st* task) {
  release_stack_reuse(task->context->uc_parent->uc_stack.ss_sp);
  release_stack_reuse(task->context->uc_child->uc_stack.ss_sp);
  thread_free(task->context->uc_parent, sizeof(ucontext_t));
  thread_free(task->context->uc_child, sizeof(ucontext_t));
  thread_free(task->context, sizeof(ptask_context_st));  
  thread_free(task, sizeof(ptask_st));
}

void release_descriptor(ptask_st* task) {
#ifdef REUSE
  release_descriptor_reuse(task);
#else
  release_descriptor_malloc(task);
#endif
}



int ptasks_sync_creator(ptask_st** task, ptask_st** tasks, int count) {
  *task = get_descriptor();
  
  (*task)->id = __sync_fetch_and_add(&rr, 1);
  (*task)->done = 0; 
  (*task)->sync_tasks_count = count;
  (*task)->sync_tasks = tasks;

  queue_add_tail(thread_queues[(*task)->id % no_threads], (void*) *task);
  return 1;
}

int ptasks_spawn(ptask_st** task, fun_t thread, param_t args, return_t* ret) {
  *task = get_descriptor();
  
  (*task)->id = __sync_fetch_and_add(&rr, 1);
  (*task)->done = 0;  
  (*task)->sync_tasks_count = 0;
  (*task)->sync_tasks = NULL;
  (*task)->args = args;
  (*task)->ret = ret;
  (*task)->impl = thread;
  
  queue_add_tail(thread_queues[(*task)->id % no_threads], (void*) *task);
  return 1;
}

void* run(void* data) {
  unsigned id = (unsigned)(data);
  printf("Running thread %u [%u] %u\n", id, TID, *(threads[id]));
  while (!stop_running) {
    task_consumer();
  }
}

ptask_st* steal(unsigned long me) {
  ptask_st* ret = NULL;
  
  for(int i = 0; i < no_threads; ++i) {
    if (i!=me && queue_count(thread_queues[i])) {
      ret = queue_remove_front(thread_queues[i]);
      if (NULL != ret) break;
    }
  }
  
  return ret;
}

void task_consumer() {
  int ret;
  unsigned long qid = no_threads + 1;
  for (int i = 0; i < 64; ++i) {
    if (*(threads[i]) == TID) {
      qid = i;
      break;
    }
  }
  if (qid < no_threads) {
    ptask_st* crt_task = (ptask_st*) queue_remove_front(thread_queues[qid]);
    
    for (int attempts = 0; attempts < 10 && NULL == crt_task; attempts++) {
      crt_task = steal(qid);
    }
    if (NULL == crt_task) {
       return;
    }

       
    getcontext((crt_task)->context->uc_child);
    (crt_task)->context->uc_child->uc_link = (crt_task)->context->uc_parent;
    if (crt_task->sync_tasks_count) {
      makecontext((crt_task)->context->uc_child, (void *)*wrapper_sync, 1, (crt_task));
    } else {  
      makecontext((crt_task)->context->uc_child, (void *)*wrapper_fun,  1, (crt_task));
    }
    
    ret = getcontext(crt_task->context->uc_parent);
    if (ret != 0) { printf("Error getting parent context: %d, %s \n", errno, strerror(errno)); }
    ret = swapcontext(crt_task->context->uc_parent, crt_task->context->uc_child);
    if (ret != 0) { printf("Error swapping context: %d, %s \n", errno, strerror(errno)); }
    
    
    if (crt_task->sync_tasks_count && !crt_task->done) {
	queue_add_tail(thread_queues[qid], (void*) crt_task);
    } else {
       jobs_done[qid]++;
    }
  }
}

void ptasks_sync(ptask_st** tasks, size_t count) {
  ptask_st* sync;
  ptasks_sync_creator(&sync, tasks, count);
    
  do {
    task_consumer();
  } while (!sync->done);

  for (int i = 0; i < sync->sync_tasks_count; i++) {
    ptask_st* sub_task = sync->sync_tasks[i];
    release_descriptor(sub_task);
  }
  release_descriptor(sync);
}

void ptask_call(fun_t thread, param_t param, return_t* ret) {
  ptask_st* sync;
  ptasks_spawn(&sync, thread, param, ret);
  while (!sync->done) {
    pthread_yield();
  }
  release_descriptor(sync);
}
