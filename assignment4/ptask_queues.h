#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>

#ifndef _PTASK_QUEUES
#define _PTASK_QUEUES


typedef enum {
  QT_JOBS = 0,
  QT_STACKS,
  QT_STRING
} QUEUE_TYPE ;

typedef struct qelem {
  void* data;
  struct qelem* next;
  struct qelem* prev;
} queue_elem_st;

typedef struct queue {
  queue_elem_st* head;
  queue_elem_st* tail;
  pthread_mutex_t* lock;
  int id;
  volatile unsigned count;
} queue_st;

queue_st* queue_init(int);
void queue_deinit(queue_st*);
void queue_print(queue_st*, QUEUE_TYPE);

void queue_add_front(queue_st*, void*);
void queue_add_tail(queue_st*, void*);
void* queue_remove_front(queue_st*);
void* queue_remove_tail(queue_st*);

unsigned queue_count(queue_st*);
void queue_lock(queue_st*);
void queue_unlock(queue_st*);

#endif