
#include <limits.h>

#ifndef LSCPU_H
#define LSCPU_H

#define CACHE_MAX 100

/* /sys paths */
#define _PATH_SYS_SYSTEM	"/sys/devices/system"
#define _PATH_SYS_CPU0		_PATH_SYS_SYSTEM "/cpu/cpu0"
#define _PATH_PROC_XEN		"/proc/xen"
#define _PATH_PROC_XENCAP	_PATH_PROC_XEN "/capabilities"
#define _PATH_PROC_CPUINFO	"/proc/cpuinfo"
#define _PATH_PROC_PCIDEVS	"/proc/bus/pci/devices"

int have_topology;
int have_cache;
int have_node;

/* cache(s) description */
struct ca_desc {
	char	*caname;
	char	*casize;
	int	camap;
};

/* virtualization types */
enum {
	VIRT_NONE	= 0,
	VIRT_PARA,
	VIRT_FULL
};
/* hypervisor vendors */
enum {
	HYPER_NONE	= 0,
	HYPER_XEN,
	HYPER_KVM,
	HYPER_MSHV
};
/* CPU modes (bits) */
enum {
	MODE_REAL	= (1 << 1),
	MODE_TRANSPARENT = (1 << 2),
	MODE_LONG	= (1 << 3)
};

static void path_scanstr(char *result, const char *path, ...)
		__attribute__ ((__format__ (__printf__, 2, 3)));
static int path_exist(const char *path, ...)
		__attribute__ ((__format__ (__printf__, 1, 2)));
static int path_sibling(const char *path, ...)
		__attribute__ ((__format__ (__printf__, 1, 2)));


/* CPU(s) description */
struct cpu_desc {
	/* counters */
	int	ct_cpu;
	int	ct_thread;
	int	ct_core;
	int	ct_socket;
	int	ct_node;
	int	ct_cache;

	/* who is who */
	char	*arch;
	char	*vendor;
	char	*family;
	char	*model;
	char	*virtflag;	/* virtualization flag (vmx, svm) */
	int	hyper;		/* hypervisor vendor ID */
	int	virtype;	/* VIRT_PARA|FULL|NONE ? */

	/* caches */
	struct ca_desc	cache[CACHE_MAX];

	/* misc */
	char	*mhz;
	char	*stepping;
	char	*flags;

	int	mode;		/* rm, lm or/and tm */

	/* NUMA */
	int	*nodecpu;
};

int lscpu_init(struct cpu_desc* cpu);

#endif /* LSCPU_H */
