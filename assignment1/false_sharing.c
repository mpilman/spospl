#define _GNU_SOURCE
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <unistd.h>
#include <sched.h>
#include "lscpu.h"

#define ptr_size (sizeof(void*))
#define payload(s) ((s - ptr_size))
#define def_no_cells (1000)
#define cells (sizeof(cell_t))
#define KB(s) (s * 1024)
#define MB(s) (s * 1024 * 1024)
#define repeat 10

struct cell16  { void* next; char p[payload(16)]; };
struct cell32  { void* next; char p[payload(32)]; };
struct cell64  { void* next; char p[payload(64)]; };
struct cell128 { void* next; char p[payload(128)]; };
struct cell256 { void* next; char p[payload(256)]; };

typedef struct cell64 cell_t;

//typedef struct { 
  char v[16*1024] __attribute__((aligned(64))); 
//} twohead_t;

typedef struct threading_param {
  //twohead_t* obj;
  char* array;
  int thread_id;
  int core;
  unsigned long long iterations;
  int delta;
} threading_param_t;

unsigned long long ticks() {
  unsigned long long res;
  __asm__ volatile (".byte 0x0f, 0x31" : "=A" (res));
  return res;
}

void thread_set_affinity(int core) {
  cpu_set_t affinity_cpu;
  CPU_ZERO(&affinity_cpu);
  CPU_SET(core, &affinity_cpu);
  if (sched_setaffinity(0, sizeof(cpu_set_t), &affinity_cpu)) {
    fprintf(stderr, "WARNING: Could not set CPU(s) Affinity (%d), will go out without this\n", errno);
  }
}



void timeval_diff(struct timeval *end, struct timeval *start) {
  end->tv_sec -= start->tv_sec;
  if (end->tv_usec < start->tv_usec) {
    --(end->tv_sec);
    end->tv_usec = start->tv_usec - end->tv_usec;
  } else {
    end->tv_usec -= start->tv_usec;
  }
}

unsigned long long detect_cache_info(unsigned long N) {
  cell_t *mem;
  posix_memalign((void*)&mem, 16, cells * N);
  for (unsigned long i = 0; i < N - 1; i++) {
    mem[i].next = (void*)&mem[i+1];
    memset(mem[i].p, cells, 1); 
  }
  mem[N-1].next = NULL;
  memset(mem[N-1].p, cells, 0);

  struct timeval tv_start, tv_end; 

  gettimeofday(&tv_start, NULL);
  unsigned long long start = ticks(); 

  for (int i = 0; i < repeat; i++) {
    cell_t* crt = mem;
    unsigned long long total = 0;
    while (crt != NULL) {
      total += crt->p[5];
      crt = (cell_t*) crt->next;
    }
  }

  unsigned long long stop = ticks();
  gettimeofday(&tv_end, NULL);
  timeval_diff(&tv_end, &tv_start);
  unsigned long long usecs = 1000 * (tv_end.tv_sec * 1000 * 1000 + tv_end.tv_usec) / repeat;

  free(mem);
  return /*usecs*/ (stop-start);
}

void run_cache_line_detection() {
    unsigned long counts[] = {KB(64)/cells,KB(128)/cells,KB(256)/cells,MB(1)/cells,MB(4)/cells,MB(8)/cells,MB(16)/cells};
    thread_set_affinity(1);
    for (int i = 0; i < sizeof(counts) / sizeof(unsigned long); i++) { 
      unsigned long long x = detect_cache_info(counts[i]) / counts[i];
      unsigned long long memsize = cells * counts[i];
      printf("cell size=%zu, cell count=%lu, total mem=%lluKB ===> %llu\n", cells, counts[i], memsize/1024, x);
    }
}


void* threading_function(void* arg) {
  threading_param_t* param = (threading_param_t*)arg;
  thread_set_affinity(param->core);
  int idx = param->thread_id * param->delta;
  printf("Doing stuff on idx=%d\n", idx);
  for (unsigned long long i = 0; i < param->iterations; ++i) {
    //param->obj->v[idx] += 1;
    param->array[idx] += 1;
  }
  return NULL;
}

int do_false_sharing(int cpus, int delta) {
  printf("Do false sharing on delta = %d, cpus=%d\n", delta, cpus);
  int core_mappings[] = {0,4,4,6,1,3,5,7};
  //twohead_t *mem = (twohead_t*)malloc(1*sizeof(twohead_t));
  threading_param_t *params[cpus];
  for (int i = 0; i < cpus; i++) {
    params[i] = (threading_param_t*)malloc(1*sizeof(threading_param_t));
    //params[i]->obj = mem;
    params[i]->array = v;
    params[i]->thread_id = i;
    params[i]->core = core_mappings[i % 4];
    params[i]->delta = delta;
    params[i]->iterations = 1000 * 1000 * 1000;
  }

  pthread_t* threads = malloc(cpus*sizeof(pthread_t));
  struct timeval begin_time;
  if (gettimeofday(&begin_time, NULL)) {
      fprintf(stderr, "ERROR: could not get current time stamp (%d): %s", errno, strerror(errno));
      return 1;
  }

  for (long i = 0; i < cpus; ++i ) {
      pthread_create(&(threads[i]), NULL, &threading_function, params[i]);
  }

  for (long i = 0; i < cpus; ++i) {
      pthread_join(threads[i], NULL);
  }
  struct timeval end_time;
  if (gettimeofday(&end_time, NULL)) {
      fprintf(stderr, "ERROR: could not get current time stamp (%d): %s", errno, strerror(errno));
      return 1;
  }
  timeval_diff(&end_time, &begin_time);
  printf("\nComplete execution time: %lds %ldus\n", end_time.tv_sec, end_time.tv_usec);
  unsigned long long xxx = end_time.tv_sec * 1000 * 1000 + end_time.tv_usec;
  printf("\n%llu\n", xxx);
}

int main(int argc, char** argv) {
  char c;
  unsigned delta = 0;
  unsigned cpus = 0;
  int do_fs = 0; 
  int do_detect = 0;
  while ((c = getopt(argc, argv, "d::f::x:c:")) != -1) {
      switch (c) {
          case 'd':
              do_detect = 1;
              break;
          case 'x':
              delta = atoi(optarg);
              break;
          case 'c':
              cpus = atoi(optarg);
              break;
          case 'f':
              do_fs = 1;
          default:
              break;
      }
  }

  if (do_detect) 
    run_cache_line_detection();
  if (do_fs)
    do_false_sharing(cpus, delta);

    

  return 0;
}
