#define _GNU_SOURCE
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <unistd.h>
#include <sched.h>
#include "lscpu.h"


void thread_set_affinity(int core) {
  cpu_set_t affinity_cpu;
  CPU_ZERO(&affinity_cpu);
  CPU_SET(core, &affinity_cpu);
  if (sched_setaffinity(0, sizeof(cpu_set_t), &affinity_cpu)) {
    fprintf(stderr, "WARNING: Could not set CPU(s) Affinity (%d), will go out without this\n", errno);
  }
}



long total_cpus_non_ht_aware_sysconf() {
  long cpus = sysconf(_SC_NPROCESSORS_ONLN);
  if (cpus <= 0) {
    fprintf(stderr, "The number of cpus returned by non_ht_aware_sysconf is invalid. Defaulting to 1 cpu.");
    return 1;
  }
  return cpus;
}

long total_cpus_non_ht_aware_sched() {
  cpu_set_t mask;
  if (sched_getaffinity(0, sizeof(cpu_set_t), &mask)) {
    fprintf(stderr, "Error while getting affinity (%d): %s. Defaulting to 1 cpu.", errno, strerror(errno));
    return 1;
  }
  return CPU_COUNT(&mask);
}

long total_cpus_ht_aware_procfs() {
  long cpus = 1;
  struct cpu_desc _cpu, *cpu = &_cpu;
  lscpu_init(cpu);
  //fprintf(stderr,"CPUs: %d\n",cpu->ct_cpu); 
  //fprintf(stderr,"Threads: %d\n",cpu->ct_thread); 
  //fprintf(stderr,"Cores: %d\n",cpu->ct_core); 
  //fprintf(stderr,"Sockets: %d\n",cpu->ct_socket); 
  return cpu->ct_socket * cpu->ct_core;
}

enum get_cpu_ways {non_ht_aware_sysconf, non_ht_aware_sched, ht_aware_procfs};

long get_total_cpus(enum get_cpu_ways w) {
  long total_cpus = 0;
  switch(w) {
    case non_ht_aware_sysconf:
      total_cpus = total_cpus_non_ht_aware_sysconf();
      break;
    case non_ht_aware_sched:
      total_cpus = total_cpus_non_ht_aware_sched();
      break;
    case ht_aware_procfs:
      total_cpus = total_cpus_ht_aware_procfs();
      break;
    default:
      fprintf(stderr, "Unknown cpu detection method. Assume default non_ht_aware sysconf.");
      total_cpus = total_cpus_non_ht_aware_sysconf();
      break;
  }
  return total_cpus;
}

unsigned long long ticks() {
    unsigned long long res;
    __asm__ volatile (".byte 0x0f, 0x31" : "=A" (res));
    return res;
}

struct restype {
    struct timeval tval;
    long* numbers;
};

struct restype *create_restype(size_t N)
{
    struct restype *res = malloc(sizeof(struct restype));
    res->numbers = malloc(N*sizeof(long)); return res;
}

void del_restype(struct restype *arg)
{
    free(arg->numbers);
    free(arg);
}

void timeval_diff(struct timeval *end, const struct timeval *begin)
{
    end->tv_sec -= begin->tv_sec;
    if (end->tv_usec < begin->tv_usec) {
        --(end->tv_sec);
        end->tv_usec = begin->tv_usec - end->tv_usec;
    } else {
        end->tv_usec -= begin->tv_usec;
    }
}

typedef struct {
  int N;
  int thread_id;
  int core;
} thread_arg_t;

void* print_randoms(void* arg) {
    thread_arg_t* tharg = (thread_arg_t*)arg;
    int N = tharg->N;
    thread_set_affinity(tharg->core);
    struct restype *res = create_restype(N);
    struct timeval begin_time;
    if (gettimeofday(&begin_time, NULL)) {
        fprintf(stderr, "ERROR: could not get current time stamp (%d): %s", errno, strerror(errno));
    }
    unsigned short xsubi[3];
    size_t last_index = 0;
    for (size_t i = 0; i*2 < sizeof(begin_time.tv_usec) && last_index < i; ++i) {
        xsubi[i] = begin_time.tv_usec >> 2*i;
    }
    for (size_t i = last_index; (i-last_index)*2 < sizeof(begin_time.tv_sec) && last_index < i; ++i) {
        xsubi[i] = begin_time.tv_sec >> 2*i;
    }
    for (int i = 0; i < N; ++i) {
        res->numbers[i] = nrand48(xsubi);
    }
    if (gettimeofday(&res->tval, NULL)) {
        fprintf(stderr, "ERROR: could not get current time stamp (%d): %s", errno, strerror(errno));
    }
    timeval_diff(&res->tval, &begin_time);
    return res;
}


int main(int argc, char** argv) {
    fprintf(stdout, "Total CPU(s) via sysconf = %ld.\n", get_total_cpus(non_ht_aware_sysconf));
    fprintf(stdout, "Total CPU(s) via sched   = %ld.\n", get_total_cpus(non_ht_aware_sched));
    fprintf(stdout, "Total CPU(s) via procfs  = %ld.\n", get_total_cpus(ht_aware_procfs));

    char c;
    unsigned N = 1000;
    long cpus = get_total_cpus(ht_aware_procfs); 
    long cpu_phys = cpus;
    char* filename = "randout";
    while ((c = getopt(argc, argv, "c:N:f:")) != -1) {
        switch (c) {
            case 'c':
                cpus = atol(optarg);
                break;
            case 'N':
                N = atoi(optarg);
                break;
            case 'f':
                filename = malloc(strlen(optarg)*sizeof(char));
                strcpy(filename, optarg);
                break;
            default:
                break;
        }
    }
    FILE *outf = fopen(filename, "w");
    pthread_t* threads = malloc(cpus*sizeof(pthread_t));
    struct restype **resvec = malloc(cpus*sizeof(struct restype*));
    struct timeval begin_time;
    if (gettimeofday(&begin_time, NULL)) {
        fprintf(stderr, "ERROR: could not get current time stamp (%d): %s", errno, strerror(errno));
        return 1;
    }
    thread_arg_t **thargs = (thread_arg_t**)malloc(cpus*sizeof(thread_arg_t*));
    int core_mappings[] = {0,2,4,6,1,3,5,7};
 
    for (long i = 0; i < cpus; ++i ) {
        thargs[i] = (thread_arg_t*)malloc(sizeof(thread_arg_t));
        thargs[i]->thread_id = i;
        thargs[i]->N = N;
        thargs[i]->core = i % cpu_phys; 
        pthread_create(&(threads[i]), NULL, &print_randoms, thargs[i]);
    }
    for (long i = 0; i < cpus; ++i) {
        struct restype *tres;
        void *res;
        pthread_join(threads[i], &res);
        tres = (struct restype *)res;
        printf("Thread %ld: %lds %ldus\n", i, tres->tval.tv_sec, tres->tval.tv_usec);
        resvec[i] = tres;
    }
    struct timeval end_time;
    if (gettimeofday(&end_time, NULL)) {
        fprintf(stderr, "ERROR: could not get current time stamp (%d): %s", errno, strerror(errno));
        return 1;
    }
    timeval_diff(&end_time, &begin_time);
    printf("\nComplete execution time: %lds %ldus\n", end_time.tv_sec, end_time.tv_usec);
    for (size_t i = 0; i < cpus; ++i) {
        struct restype *tres = resvec[i];
        for (size_t j = 0; j < N; ++j) {
            fprintf(outf, "%ld\n", tres->numbers[j]);
        }
        del_restype(tres);
    }
    free(resvec);
    free(thargs);
    free(threads);
    fclose(outf);
    
    return 0;
}
