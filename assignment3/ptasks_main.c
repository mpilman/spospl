#include "ptasks.h"
#include "stdio.h"
#include "thread_malloc.h"

param_t N;

return_t fib(param_t n) {
  return_t final = n;
  printf("Doing fib(%d) from TID=%u\n", n, TID);
  if (n < 2)  {
    printf("Done fib(%d) = %d from TID=%u\n", n, final, TID);
    return final;
  }
  return_t x, y;
  ptask_st* tasks[2];
  ptasks_spawn(&tasks[0], &fib, n-1, &x);
  ptasks_spawn(&tasks[1], &fib, n-2, &y);
  ptasks_sync(tasks, 2);
  final = x+y;
  printf("Done fib(%d)=%d from TID=%u\n", n, final, TID);
  return final;
}


void* pmain(void* arg) {
  ptasks_init(2,2);
  
  return_t n;
  ptask_call(&fib, N, &n);
  printf("Fib(%lu) = %lu \n", N, n);
  
  ptasks_deinit();
}

int main(int argc, char** argv) {
  thread_malloc_init();
  if (argc > 1)
    N = atoi(argv[1]);
  else 
    N = 10;
  
  pthread_attr_t attr;
  size_t stacksize;
  pthread_t pseudomain;
  
  pthread_attr_init(&attr);
  pthread_attr_getstacksize (&attr, &stacksize);
  stacksize = sizeof(void*) * 100 * 1024 * 1024;
  pthread_attr_setstacksize (&attr, stacksize);
  
  pthread_create(&pseudomain, &attr, pmain, NULL);
  pthread_join(pseudomain, NULL);

  return 0;
}
