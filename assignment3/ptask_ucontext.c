#include "ptasks.h"
#include "thread_malloc.h"

queue_st* thread_queues[64];
pthread_t* threads[64];
volatile int stop_running;
int no_cores;
int no_threads;
volatile unsigned rr;

void* run (void*);
void task_consumer (unsigned);

void print_stack() {
  void *trace[32];
  char **messages = (char **)NULL;
  int i, trace_size = 0;

  trace_size = backtrace(trace, 32);
  messages = backtrace_symbols(trace, trace_size);
  for (i=0; i<trace_size; ++i) {
    //printf("[bt] #%d %s\n", i, messages[i]);
    char syscom[256];
    sprintf(syscom,"echo -e \"\t%u\t%s\" | c++filt", TID, messages[i]); 
    system(syscom);
  }

}

void ptask_print(ptask_st* task) {
  printf("\tid=%d, done=%d, sync=%d\n", task->id, task->done, task->sync_tasks_count);
  ptask_st** tasks = task->sync_tasks;
  for (int i = 0; i < task->sync_tasks_count; i++) {
    printf("\t\t id = %d, done = %d, arg = %u\n", tasks[i]->id, tasks[i]->done, tasks[i]->args);
  }
  printf("\tTask %d with stack pointers child = %p, parent = %p and contexts child = %p, parent = %p, link = %p \n", 
	 (task)->id, 
	 (task)->context->uc_child->uc_stack.ss_sp, (task)->context->uc_parent->uc_stack.ss_sp,
	 (task)->context->uc_child, (task)->context->uc_parent, (task)->context->uc_child->uc_link);
}

void wrapper_fun(ptask_st* task) {
  return_t ret = task->impl(task->args);
  *(task->ret) = ret;
  task->done = 1;
  printf("Done a wrapper fun, before set context\n");
  print_stack();
  setcontext(task->context->uc_parent);
}

void ptasks_init(unsigned _no_threads, unsigned _no_cores) { 
  stop_running = 0;
  no_cores = _no_cores;
  no_threads = _no_threads;
  for (unsigned i = 0; i < no_threads; i++) {
    thread_queues[i] = queue_init();
    threads[i] = (pthread_t*)thread_malloc(sizeof(pthread_t));
    pthread_create(threads[i], NULL, run, (void*) i);
  }
  rr = 0;
}

void ptasks_deinit() {
  for (int i = 0; i < no_threads; ++i) {
    queue_print(thread_queues[i], QT_JOBS);
  }
  
  //queue_deinit(stacks);
  stop_running = 1;
  for (int i = 0; i < no_threads; i++) {
    pthread_join(*threads[i], NULL);
  }
  for (int i = 0; i < no_threads; i++) {
    queue_deinit(thread_queues[i]);
    thread_free(threads[i], sizeof(pthread_t));
  }
}

void ptasks_sync_checker(ptask_st* sync_task) {
  int all_done = 1;
  for (size_t i = 0; i < sync_task->sync_tasks_count && all_done; ++i) {
    if (!(sync_task->sync_tasks[i]->done)) {
      all_done = 0;
    }
  }
  if (all_done)  {
    sync_task->done = 1;
    setcontext(sync_task->context->uc_parent);
  } else {
    task_consumer(sync_task->id % no_threads);
    setcontext(sync_task->context->uc_parent);
  }
}

int ptasks_sync_creator(ptask_st** task, ptask_st** tasks, int count) {
  *task = (ptask_st*) thread_malloc(sizeof(ptask_st));
  
  (*task)->done = 0;
  (*task)->sync_tasks_count = count;
  (*task)->sync_tasks = tasks;
  (*task)->id = __sync_add_and_fetch(&rr, 1);; 
  (*task)->context = (ptask_context_st*) thread_malloc(sizeof(ptask_context_st));
  (*task)->context->uc_child = (ucontext_t*) thread_malloc(sizeof(ucontext_t));
  (*task)->context->uc_parent = (ucontext_t*) thread_malloc(sizeof(ucontext_t));
  printf("Sync spawned with id = %u\n", (*task)->id);
  
  (*task)->context->uc_child->uc_stack.ss_flags = 0;
  (*task)->context->uc_child->uc_stack.ss_size = STACK_SIZE;
  (*task)->context->uc_child->uc_stack.ss_sp = thread_malloc(STACK_SIZE);
  memset((*task)->context->uc_child->uc_stack.ss_sp, 0, STACK_SIZE);
  
    
  (*task)->context->uc_parent->uc_stack.ss_flags = 0;
  (*task)->context->uc_parent->uc_stack.ss_size = STACK_SIZE;
  (*task)->context->uc_parent->uc_stack.ss_sp = thread_malloc(STACK_SIZE);
  memset((*task)->context->uc_parent->uc_stack.ss_sp, 0, STACK_SIZE);
  
  getcontext((*task)->context->uc_child);
  (*task)->context->uc_child->uc_link = (*task)->context->uc_parent;  
  makecontext((*task)->context->uc_child, (void *)*ptasks_sync_checker, 1, (*task));
  queue_add(thread_queues[(*task)->id % no_threads], (void*) *task);
 
  return 1;
}

int ptasks_spawn(ptask_st** task, fun_t thread, param_t args, return_t* ret) {
  *task = (ptask_st*) thread_malloc(sizeof(ptask_st));
  
  (*task)->args = args;
  (*task)->ret = ret;
  (*task)->impl = thread;
  (*task)->done = 0;
  (*task)->sync_tasks_count = 0;
  (*task)->sync_tasks = NULL;
  (*task)->id = __sync_add_and_fetch(&rr, 1);
  printf("Got spawned with id = %u\n", (*task)->id);
  
  (*task)->context = (ptask_context_st*) thread_malloc(sizeof(ptask_context_st));
  (*task)->context->uc_child = (ucontext_t*) thread_malloc(sizeof(ucontext_t));
  (*task)->context->uc_parent = (ucontext_t*) thread_malloc(sizeof(ucontext_t));
  
  
  (*task)->context->uc_child->uc_stack.ss_flags = 0;
  (*task)->context->uc_child->uc_stack.ss_size = STACK_SIZE;
  (*task)->context->uc_child->uc_stack.ss_sp = thread_malloc(STACK_SIZE);
  memset((*task)->context->uc_child->uc_stack.ss_sp, 0, STACK_SIZE);
  
  (*task)->context->uc_parent->uc_stack.ss_flags = 0;
  (*task)->context->uc_parent->uc_stack.ss_size = STACK_SIZE;
  (*task)->context->uc_parent->uc_stack.ss_sp = thread_malloc(STACK_SIZE);
  memset((*task)->context->uc_parent->uc_stack.ss_sp, 0, STACK_SIZE);
  
  getcontext((*task)->context->uc_child);
  (*task)->context->uc_child->uc_link = (*task)->context->uc_parent;
  makecontext((*task)->context->uc_child, (void *)*wrapper_fun,         1, (*task));
  queue_add(thread_queues[(*task)->id % no_threads], (void*) *task);
  return 1;
}

void* run(void* data) {
  unsigned id = (unsigned)(data);
  printf("Running thread %u [%u]\n", id, TID);
  while (!stop_running) {
    task_consumer(id);
  }
}

void task_consumer (unsigned qid) {
  int ret;
  if (qid < no_threads) {
    ptask_st* crt_task = (ptask_st*) queue_remove(thread_queues[qid]);
    if (NULL == crt_task) {
      // nothing to consume of our own task
      return;
    }
      
    ret = getcontext(crt_task->context->uc_parent);
    if (ret != 0) { printf("Error getting parent context: %d, %s \n", errno, strerror(errno)); }
    ret = swapcontext(crt_task->context->uc_parent, crt_task->context->uc_child);
    if (ret != 0) { printf("Error swapping context: %d, %s \n", errno, strerror(errno)); }
    printf("Jumped back\n");
    
    if (crt_task->sync_tasks_count && !crt_task->done) {
	queue_add(thread_queues[qid], (void*) crt_task);
    }
  }
}

void ptasks_sync(ptask_st** tasks, size_t count) {
    ptask_st* sync;
    ptasks_sync_creator(&sync, tasks, count);

    do {
        task_consumer(sync->id % no_threads);
    } while (!sync->done);

    for (int i = 0; i < count; i++) {
        ptask_st* sub_task = tasks[i];
        thread_free(sub_task->context->uc_parent->uc_stack.ss_sp, STACK_SIZE);
        thread_free(sub_task->context->uc_parent, sizeof(ucontext_t));
        thread_free(sub_task->context->uc_child->uc_stack.ss_sp, STACK_SIZE);
        thread_free(sub_task->context->uc_child, sizeof(ucontext_t));
        thread_free(sub_task->context, sizeof(ptask_context_st));
        thread_free(sub_task, sizeof(ptask_st));
    }
    thread_free(sync->context->uc_parent->uc_stack.ss_sp, STACK_SIZE);
    thread_free(sync->context->uc_child->uc_stack.ss_sp, STACK_SIZE);
    thread_free(sync->context->uc_parent, sizeof(ucontext_t));
    thread_free(sync->context->uc_child, sizeof(ucontext_t));
    thread_free(sync->context, sizeof(ptask_context_st));  
    thread_free(sync, sizeof(ptask_st));
}

void ptask_call(fun_t thread, param_t param, return_t* ret) {
    ptask_st* sync;
    ptasks_spawn(&sync, thread, param, ret);
    while (!sync->done) {
        pthread_yield();
    }
    thread_free(sync->context->uc_parent->uc_stack.ss_sp, STACK_SIZE);
    thread_free(sync->context->uc_child->uc_stack.ss_sp, STACK_SIZE);
    thread_free(sync->context->uc_parent, sizeof(ucontext_t));
    thread_free(sync->context->uc_child, sizeof(ucontext_t));
    thread_free(sync->context, sizeof(ptask_context_st));  
    thread_free(sync, sizeof(ptask_st));
}
