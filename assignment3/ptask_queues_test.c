#include "ptask_queues.h"

int main(int argc, char** argv) {
 
  queue_st* q = queue_init();
  for (int i = 0; i < 10; i++) {
    char* c = (char*)malloc(256*1024*sizeof(char));
    sprintf(c, "This is a test %d", i);
    queue_add(q, c);
  }
  
  for (int i = 0; i < 59000011; i++) {
    //queue_print(q, QT_STRING);
    char* x = (char*)queue_remove(q);
    queue_add(q, (void*)x);
  }
  
  queue_print(q, QT_STRING);
  
  queue_deinit(q);
  
  return 1;
}