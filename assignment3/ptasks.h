//This file defines the header implementing parallel tasks on top of a single thread.
//Implicitly it allows scheduling on top of one thread.

#include <stdlib.h>
#include <unistd.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <execinfo.h>
#include <signal.h>

#define __USE_GNU
#include <pthread.h>
#undef __USE_GNU

#include <stdio.h>
#include <ucontext.h>
#include "ptask_queues.h"

#ifndef _PTASKS
#define _PTASKS

#define TID ((unsigned int)pthread_self())

#define MAX_STACKS 10000
#define STACK_SIZE_JMP (1*1024)
#define STACK_SIZE_UC (8*1024)
#define STACK_SIZE STACK_SIZE_JMP

//#define USE_MALLOC
#define USE_STATIC

#ifdef USE_STATIC
queue_st* stacks;
#endif



void print_stack();

typedef unsigned long long param_t;
typedef unsigned long long return_t;
typedef return_t (*fun_t)(param_t);

typedef struct {
  // these are used for the ucontext approach
  ucontext_t* uc_child;
  ucontext_t* uc_parent;  
} ptask_context_st;

typedef struct ptask {
  fun_t impl;
  param_t args;
  return_t* ret;
  
  volatile int done;
  unsigned id;

  ptask_context_st* context;
  
  struct ptask** sync_tasks;
  int sync_tasks_count;
  
} ptask_st;


void	ptasks_init(unsigned no_threads, unsigned no_cores);
void	ptasks_deinit();
int	ptasks_spawn(ptask_st** ptask, fun_t thread, param_t param, return_t* ret);
void	ptasks_sync(ptask_st** tasks, size_t count);
void 	ptasks_return(return_t ret);
void	ptask_call(fun_t thread, param_t param, return_t* ret);
void	ptask_print(ptask_st* task);

#endif