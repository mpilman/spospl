#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>

#ifndef _PTASK_QUEUES
#define _PTASK_QUEUES


typedef enum {
  QT_JOBS = 0,
  QT_STACKS,
  QT_STRING
} QUEUE_TYPE ;

typedef struct qelem {
  void* data;
  struct qelem* next;
} queue_elem_st;

typedef struct queue {
  queue_elem_st* head;
  queue_elem_st* tail;
  pthread_mutex_t* lock;
} queue_st;

queue_st* queue_init(void);
void queue_deinit(queue_st*);
void queue_print(queue_st*, QUEUE_TYPE);
void queue_add(queue_st*, void*);
void* queue_remove(queue_st*);
int queue_empty(queue_st*);

#endif