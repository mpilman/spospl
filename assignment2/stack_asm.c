#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <setjmp.h>

#define GETSP(x) __asm__ ("mov %%rsp,%0 ":"=g"(x) )
#define GETBP(x) __asm__ ("mov %%rbp,%0 ":"=g"(x) )
//#define SETSP(x) __asm__ ("mov %1,%%rsp;mov %%rsp,%0 " : "=g"(x) : "r"(x))
#define SETSP(x) __asm__ ("mov %1,%%rsp;mov %%rsp,%0 " : "=g"(x) : "r"(x))
#define SETBP(x) __asm__ ("mov %1,%%rbp;mov %%rbp,%0 " : "=g"(x) : "r"(x))

#define STAT_INIT long rsp, rbp;

#define STAT \
  GETSP(rsp); \
  GETBP(rbp); \
  printf("%s: Got the rsp = %p, rbp= = %p, diff = %zu\n", __FUNCTION__, rsp, rbp, rbp - rsp);
#define N 1024



jmp_buf parent, child;
long *mem;
  
void y() {
  STAT_INIT;
  STAT;
}



int x(int x, int y) {
 int z = 0;
 //printf("%s: %d %d", __FUNCTION__, x, y);
 z = x+y;
 return z;
}
  
int main(int argc, char** argv) {
  STAT_INIT;
  mem = (long*) malloc(N*sizeof(long));
  memset(mem, 'J'-'A', N*sizeof(long));
  STAT;
  
  long new_sp = &(mem[N]);
  for (int i = 0; i < N; i++) { mem[i] = 16*16*16; }
  //mem[N-1] = rbp;
  
  //printf("Got here with new-sp = %p, when mem = %p and last val = %p\n", new_sp, mem, *((long*)new_sp - 8));
  GETSP(rsp);
  SETSP(new_sp);
  int ret = x(5,6);
  SETSP(rsp);
  
  printf("Got the ret val = %d;\n", ret);
  free(mem);
}