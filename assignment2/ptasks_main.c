#include "ptasks.h"
#include "stdio.h"

param_t N;

void fib(param_t n) {
  return_t final = n;
  if (n < 2)  {
    ptasks_return(final);
  }
  return_t x, y;
  ptask_st* tasks[2];
  ptasks_spawn(&tasks[0], &fib, n-1, &x);
  ptasks_spawn(&tasks[1], &fib, n-2, &y);
  ptasks_sync(tasks, 2);
  final = x+y;
  ptasks_return(final);
}


void* pmain(void* arg) {
  ptasks_init(0,0);
  
  return_t n;
  ptask_st *t[1];

  ptasks_spawn(&t[0], &fib, N, &n);
  ptasks_sync(t, 1);
  printf("Fib(%lu) = %lu \n", N, n);
}

int main(int argc, char** argv) {
  if (argc > 1)
    N = atoi(argv[1]);
  else 
    N = 10;
  
  pthread_attr_t attr;
  size_t stacksize;
  pthread_t pseudomain;
  
  pthread_attr_init(&attr);
  pthread_attr_getstacksize (&attr, &stacksize);
  stacksize = sizeof(void*) * 100 * 1024 * 1024;
  pthread_attr_setstacksize (&attr, stacksize);
  
  pthread_create(&pseudomain, &attr, pmain, NULL);
  pthread_join(pseudomain, NULL);

  return 0;
}
