#include "ptasks.h"

ptask_st* crt_tasks[MAX_STACKS];
unsigned next_task_top = 0;


void wrapper_fun(ptask_st* local) {
   crt_tasks[next_task_top-1]->impl(crt_tasks[next_task_top-1]->args);
}

void ptasks_init(unsigned no_threads, unsigned no_cores) {
#ifdef USE_STATIC
  stacks = malloc(MAX_STACKS * STACK_SIZE);
#endif
}

void ptasks_deinit() {
#ifdef USE_STATIC
  free(stacks);
#endif
}

void ptasks_return(return_t ret) {
  *(crt_tasks[next_task_top-1]->ret) = ret;
  crt_tasks[next_task_top-1]->done = 1;
  next_task_top--;
  setcontext(crt_tasks[next_task_top]->context->uc_parent);
}

int ptasks_spawn(ptask_st** ptask, fun_t thread, param_t args, return_t* ret) {
  *ptask = (ptask_st*) malloc(sizeof(ptask_st));
  ptask_st* crt_task = *ptask;
  
  crt_task->args = args;
  crt_task->ret = ret;
  crt_task->impl = thread;
  crt_task->done = 0;
  crt_task->id = next_task_top;
  crt_task->context = (ptask_context_st*) malloc(sizeof(ptask_context_st));
  crt_task->context->uc_child = (ucontext_t*) malloc(sizeof(ucontext_t));
  crt_task->context->uc_parent = (ucontext_t*) malloc(sizeof(ucontext_t));
  crt_tasks[next_task_top++] = crt_task;
  
  crt_task->context->uc_child->uc_stack.ss_flags = 0;
  crt_task->context->uc_child->uc_stack.ss_size = STACK_SIZE;
#ifdef USE_MALLOC  
  crt_task->context->uc_child->uc_stack.ss_sp = malloc(STACK_SIZE);
#else
  crt_task->context->uc_child->uc_stack.ss_sp = stacks + ( (2*(next_task_top-1)) * STACK_SIZE);
#endif
  //sigaltstack(&crt_task->context->uc_child->uc_stack, (stack_t *)0);
  
  crt_task->context->uc_parent->uc_stack.ss_flags = 0;
  crt_task->context->uc_parent->uc_stack.ss_size = STACK_SIZE;
#ifdef USE_MALLOC
  crt_task->context->uc_parent->uc_stack.ss_sp = malloc(STACK_SIZE);
#else
  crt_task->context->uc_parent->uc_stack.ss_sp = stacks + ( (2*(next_task_top-1) + 1) * STACK_SIZE);
#endif
  //sigaltstack(&crt_task->context->uc_parent->uc_stack, (stack_t *)0);
  
  getcontext(crt_task->context->uc_child);
  makecontext(crt_task->context->uc_child, (void *)*wrapper_fun, 1, crt_task);
  
  getcontext(crt_task->context->uc_parent);
  swapcontext(crt_task->context->uc_parent, crt_task->context->uc_child);

#ifdef USE_MALLOC
  free(crt_task->context->uc_parent->uc_stack.ss_sp);
#endif
  free(crt_task->context->uc_parent);
#ifdef USE_MALLOC  
  free(crt_task->context->uc_child->uc_stack.ss_sp);
#endif  
  free(crt_task->context->uc_child);
  free(crt_task->context);
  
  return 1;
}

void ptasks_sync(ptask_st** tasks, size_t count) {
  int all_done = 0;
  while (!all_done) {
    all_done = 1;
    for (size_t i = 0; i < count && all_done; ++i) {
      if (!(tasks[i]->done))  all_done = 0;
    }
    if (all_done) break;
    usleep(1000);
  }
  for (size_t i = 0; i < count; ++i) free(tasks[i]);
}