#include "ptasks.h"

ptask_st* crt_tasks[MAX_STACKS];
unsigned next_task_top = 0;
//#define  crt_task (crt_tasks[next_task_top-1]) 


void task_sig_handler(int arg) {
  if (setjmp(crt_tasks[next_task_top-1]->context->child)) {
    (crt_tasks[next_task_top-1]->impl(crt_tasks[next_task_top-1]->args));
  }
  return;
}

void ptasks_return(return_t ret) {
  *(crt_tasks[next_task_top-1]->ret) = ret;
  if (next_task_top > 0) {
    ptask_context_st* sched = crt_tasks[next_task_top-1]->context;
    next_task_top--;
    longjmp(sched->parent, 1);
  } 
}

void ptasks_init(unsigned no_threads, unsigned no_cores) {
#ifdef USE_STATIC
  stacks = malloc(MAX_STACKS * STACK_SIZE);
#endif
}

void ptasks_deinit() {
#ifdef USE_STATIC
  free(stacks);
#endif
}

int ptasks_spawn(ptask_st** ptask, fun_t thread, param_t args, return_t* ret) {
  *ptask = (ptask_st*) malloc(sizeof(ptask_st));
  ptask_st* crt_task = *ptask;
  crt_task->args = args;
  crt_task->ret = ret;
  crt_task->impl = thread;
  crt_task->done = 0;
  crt_task->context = (ptask_context_st*) malloc(sizeof(ptask_context_st));
  crt_tasks[next_task_top++] = crt_task;
  //next_task_top++;
  
  
  /* Setup the new stack for this task */
  crt_task->context->stack.ss_flags = 0;
  crt_task->context->stack.ss_size = STACK_SIZE;
#ifdef USE_MALLOC
  crt_task->context->stack.ss_sp = malloc(STACK_SIZE);
#else
  crt_task->context->stack.ss_sp = stacks + ((next_task_top-1) * STACK_SIZE);
#endif
  sigaltstack(&crt_task->context->stack, (stack_t *)0);
  
  /* Setup the signal action */
  crt_task->context->sa.sa_handler = &task_sig_handler;
  crt_task->context->sa.sa_flags = SA_ONSTACK;
  sigemptyset(&(crt_task->context->sa.sa_mask));
  sigaction( SIGUSR1, &(crt_task->context->sa), 0 );
  raise(SIGUSR1);
  
  
  if (setjmp(crt_task->context->parent)) {
    // the code will return here when threading function is done
    crt_tasks[next_task_top]->done = 1;
#ifdef USE_MALLOC
    free(crt_tasks[next_task_top]->context->stack.ss_sp);
#endif
    free(crt_tasks[next_task_top]->context);
  } else {
    longjmp(crt_task->context->child, 1);
  }

  return 1;
}

void ptasks_sync(ptask_st** tasks, size_t count) {
  int all_done = 0;
  while (!all_done) {
    all_done = 1;
    for (size_t i = 0; i < count && all_done; ++i) {
      if (!(tasks[i]->done))  all_done = 0;
    }
    if (all_done) break;
    usleep(1000);
  }

  for (size_t i = 0; i < count; ++i) free(tasks[i]);
}
