#include <iostream>
#include <unistd.h>
#include <omp.h>

using namespace std;

int len = 1000;

int main(void)
{
    int *a = new int[len];
#pragma omp parallel for schedule(dynamic)
    for (int i = 0; i < len; ++i) {
        if (i < 10) {
            usleep(10000);
        }
        a[i] = i;
    }
}
