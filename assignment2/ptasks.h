//This file defines the header implementing parallel tasks on top of a single thread.
//Implicitly it allows scheduling on top of one thread.

#include <setjmp.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <signal.h>
#include <ucontext.h>

#define GETSP(x) __asm__ ("mov %%rsp,%0 ":"=g"(x) )
#define GETBP(x) __asm__ ("mov %%rbp,%0 ":"=g"(x) )
#define SETSP(x) __asm__ ("mov %1,%%rsp;mov %%rsp,%0 " : "=g"(x) : "r"(x))
#define SETBP(x) __asm__ ("mov %1,%%rbp;mov %%rbp,%0 " : "=g"(x) : "r"(x))

#define MAX_STACKS 1000
#define STACK_SIZE_JMP (64*1024)
#define STACK_SIZE_UC (8*1024)
#define STACK_SIZE STACK_SIZE_JMP

//#define USE_MALLOC
#define USE_STATIC

#ifdef USE_STATIC
void* stacks;
#endif

typedef unsigned long long param_t;
typedef unsigned long long return_t;
typedef void (*fun_t)(param_t);

typedef struct {
  // these are used for the setjmp/longjmp approach
  struct sigaction sa;
  stack_t stack;
  jmp_buf child;
  jmp_buf parent;
  
  // these are used for the ucontext approach
  ucontext_t* uc_child;
  ucontext_t* uc_parent;
  
} ptask_context_st;

typedef struct {
  fun_t impl;
  param_t args;
  return_t* ret;
  
  int done;
  int id;

  ptask_context_st* context;
} ptask_st;


void	ptasks_init(unsigned no_threads, unsigned no_cores);
void	ptasks_deinit();
int	ptasks_spawn(ptask_st** ptask, fun_t thread, param_t param, return_t* ret);
void	ptasks_sync(ptask_st** tasks, size_t count);
void 	ptasks_return(return_t ret);
