#include "uthread.h"

int fib(int n)
{
    int res = n;
    fib_impl(&res);
    return res;
}

void *fib_impl(void *nptr)
{
    int n = *((int *)nptr);
    if (n < 2) return nptr;
    int x, y;
    int n_1 = n - 1;
    int n_2 = n - 2;
    uthread_t u1, u2;
    uthread_create(&u1, &fib_impl, (void*) &n_1);
    uthread_create(&u2, &fib_impl, (void*) &n_2);
    void* dummy_ptr;
    uthread_join(&u1, &dummy_ptr);
    uthread_join(&u2, &dummy_ptr);
    *((int *)nptr) = u1 + u2;
    return nptr;
}