#pragma once

typedef struct {
    void *impl;
    ucontext_t context;
} uthread_t;

/**
 * \brief Initializes the uthread library.
 *
 * This function initializes the uthread library. It must be called
 * before the first call to uthread_create. It will start num_procs
 * processes (kernel level threads) and initialize the sceduling
 * queue. If num_processes is 0, the function will set it to the
 * number of cores on the current system. Since the scheduler uses
 * a lock free queue, the user must define an upper limit of maximal
 * threads running at the time on a system. The default value (when
 * 0 is given as an argument will be 1024.
 *
 * \param num_procs     The number of kernel level threads that
 *                      should be started.
 * \param max_threads   The maximal number of threads a program
 *                      is allowed to run.
 * \return              0 on success, -1 otherwise.
 */
int uthread_init(unsigned num_procs, unsigned max_threads);

/**
 * \brief Creates a new user level thread.
 *
 * This function will create a new user level thread, which will
 * run the given function. uthread_create will return immidiately
 * returning an 0 on success and -1 otherwise.
 *
 * \param thread        The context of the user level thread;
 * \param start_routine The routine which will get executed in the
 *                      user level thread.
 * \param arg           The argument which will be given to the
 *                     start_routine at execution time.
 * \return              0 on success, -1 otherwise
 */
int uthread_create(uthread_t *thread,
                   void *(*start_routine)(void *),
                   void *arg);

/**
 * \brief Joins the given user level thread.
 *
 * The current thread will stop execution and wait for the given
 * thread to finish. The return value of the thread will be
 * written to
 * value_ptr.
 *
 * \param thread    The thread to wait for.
 * \param value_ptr A pointer where the return value should be
 *                  written to.
 *
 * \return          0 on success, -1 otherwise.
 */
int uthread_join(uthread_t *thread, void **value_ptr);